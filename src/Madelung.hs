module Madelung
  (madelung
  ) where

import Data.Massiv.Array as Massiv hiding (read)
import Numeric.Natural

-- | Numerically calculates the Madelung constant for a 2D NaCl lattice. The only parametre is \(m\)
-- and a lattice of size \(2m + 1 \times 2m + 1\) with the atom of interest at \(m, m\) will be
-- evaluated.
madelung :: (Unbox a, RealFloat a) => Natural -> a
madelung m =
  let coulombContrib = flip Massiv.imap grid $ \ix@(r :. c) fac -> if r == m' && c == m'
        then 0
        else fac / distF ix
  in  Massiv.sum $ coulombContrib
  where
    m' = fromIntegral m

    -- | Calculates an abstract distance to the centre of the lattice.
    distF :: (RealFloat a) => Ix2 -> a
    distF (r :. c) = sqrt . fromIntegral $ (r - m')^(2 :: Int) + (c - m')^(2 :: Int)

    -- | Builds a valid latice with an odd number of rows and columns. This is a square matrix and
    -- encodes the weights of coulomb contributions. The ion of interest is in the centre.
    grid :: (Unbox a, RealFloat a) => Matrix D a
    grid = makeArray Par (Sz $ n :. n) ixF
      where
        n = fromIntegral $ 2 * m + 1
        ixF (r :. c)
          | r == 0 && c == 0 = sign * corner
          | r == n - 1 && c == n - 1 = sign * corner
          | r == 0 && c == n - 1 = sign * corner
          | r == n -1 && c == 0 = sign * corner
          | r == 0 = sign * edge
          | r == n - 1 = sign * edge
          | c == 0 = sign * edge
          | c == n - 1 = sign * edge
          | otherwise = sign
          where
            edge = 1 / 2
            corner = 1 / 4
            sign = case (even r, even c) of
              (True, True) -> -1
              (True, False) -> 1
              (False, True) -> 1
              (False, False) -> -1
