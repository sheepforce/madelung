import Madelung
import System.Environment
import Text.Read
import Control.Exception
import Numeric.Natural

-- | Call from the command line with a single natural number as argument.
main :: IO ()
main = do
  args <- getArgs
  m <- case traverse (readMaybe :: String -> Maybe Natural) args of
        Just [a1] -> return a1
        Just _ -> throwIO . PatternMatchFail $ "Error: wrong number of arguments. Call with exactly one natural number."
        Nothing -> throwIO . PatternMatchFail $ "Error: Cannot parse arguments. Pass exactly one natural number."
  let res = madelung @Double m
  putStrLn $ "The Madelung M(m=" <> show m <> ")=" <> show res
