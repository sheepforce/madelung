let
  pkgs = import ./pkgs.nix;
  haskellEnv = pkgs.haskellPackages.ghcWithPackages (p: with p; [
    brittany
    rio
    massiv_1_0_1_1
  ]);
in with pkgs; mkShell {
  buildInputs = [
    haskellEnv
    cabal-install
    haskell-language-server
  ];
}
