let
  sources = import ./sources.nix;
  nixpkgs = import sources.nixpkgs {
    overlays = [ haskellFixOvl ];
  };
  haskellFixOvl = self: super: {
    haskellPackages = super.haskellPackages.extend (hself: hsuper: {
       massiv_1_0_1_1 = hsuper.callHackageDirect {
         pkg = "massiv";
         ver = "1.0.1.1";
         sha256 = "sha256-oCvpv9t6Nr+ZkRRfFUB2yNfMEgQFKg5SYB6rd/oC6+k=";
       } { scheduler = hself.scheduler_2_0_0_1; };

       optics_0_4 = hsuper.callHackageDirect {
         pkg = "optics";
         ver = "0.4";
         sha256 = "lBf9sbicZCX6PydAsd2yB0D+oM3uxaLFUivewQt08/A=";
       } {
         optics-core = hself.optics-core_0_4;
         optics-extra = hself.optics-extra_0_4;
         optics-th = hself.optics-th_0_4;
       };

       madelung = hsuper.callCabal2nix "madelung" (super.lib.cleanSource ../.) {
         massiv = hself.massiv_1_0_1_1;
       };
     });
   };

in nixpkgs
